# coding: utf8

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
#########################################################################

if request.env.web2py_runtime_gae:            # if running on Google App Engine
    db = DAL('gae')                           # connect to Google BigTable
    session.connect(request, response, db=db) # and store sessions and tickets there
    ### or use the following lines to store sessions in Memcache
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db=MEMDB(Client())
else:                                         # else use a normal relational database
    db = DAL('sqlite://storage.sqlite')       # if not, use SQLite or other DB
## if no need for session
# session.forget()

#table defns

#auth
from gluon.tools import Auth
## instantiate the Auth class (or your derived class)
auth=Auth(globals(),db)
#auth.settings.table_user=db.define_table(
#	auth.settings.table_user_name,
#	Field('user_name',length=128,default='',requires=[IS_NOT_IN_DB(db,'%s.user_name'%auth.settings.table_user_name)]),
#	Field('first_name', length=128, default=''),
#	Field('last_name',length=128, default=''),
#	Field('email',length=128,default='',requires=[IS_EMAIL(), IS_NOT_IN_DB(db,'%s.email'%auth.settings.table_user_name)]),
#	Field('password', 'password', readable=False, label='Password', requires=CRYPT()),
#	Field('registration_key', length=128, writable=False, readable=False,default=''))
##create all necessary tables
auth.define_tables()

#categories
db.define_table('category',
				SQLField('is_income','boolean'),
				SQLField('name'),
				SQLField('sort_order', 'integer')
				)
#paid_by
db.define_table('paid_by',
				SQLField('paid_by','string')
				)
								
#stores exp income record				
db.define_table('eirecord',
			   SQLField('rec_date', 'date'),
			   SQLField('description', 'text', length=100),
			   SQLField('amount','double'),
			   SQLField('is_income', 'boolean'), #if income y, else expense			   
			   SQLField('category',db.category), #lunch, travel etc
			   SQLField('paid_by',db.paid_by),
			   SQLField('created_on', 'datetime'),
			   SQLField('created_by',db.auth_user),
			   SQLField('created_via','string')
			   )

#config (system admin)
db.define_table('config',
	SQLField('twitter_id'),
	SQLField('twitter_pwd'),
	SQLField('last_twitter_intime', 'datetime'),
	SQLField('last_dm_id','integer')
	)

##insert dummy records##
if len(db().select(db.category.ALL))==0:
    db.category.insert(is_income=1,name='salary', sort_order=1)
    db.category.insert(is_income=1,name='others', sort_order=2)
    db.category.insert(is_income=0,name='lunch', sort_order=1)	

if len(db().select(db.paid_by.ALL))==0:
    db.paid_by.insert(paid_by='Cash')
    db.paid_by.insert(paid_by='Credit Card')
    db.paid_by.insert(paid_by='Cheque')
        
if len(db().select(db.auth_group.ALL)) == 0:
    db.auth_group.insert(role='users')
    db.auth_group.insert(role='admin')  		  

#insert config data
if len(db().select(db.config.ALL))==0:
        import datetime
        db.config.insert(twitter_id='yatsy', twitter_pwd='changepwd', last_twitter_intime=datetime.datetime.now(), last_dm_id=0)
        
#insert an admin user
from gluon.utils import md5_hash
if len(db().select(db.auth_user.ALL)) == 0:
    db.auth_user.insert(first_name='user', last_name='1', email='user@example.com', password=md5_hash('web2py'))
    db.auth_user.insert(first_name='admin', last_name='', email='admin@example.com', password=md5_hash('web2py'))
    
if len(db().select(db.auth_membership.ALL)) == 0:
    db.auth_membership.insert(user_id=db(db.auth_user.email=='user@example.com').select(db.auth_user.ALL)[0].id, group_id=db(db.auth_group.role=='users').select(db.auth_group.id)[0].id)
    db.auth_membership.insert(user_id=db(db.auth_user.email=='admin@example.com').select(db.auth_user.ALL)[0].id, group_id=db(db.auth_group.role=='admin').select(db.auth_group.id)[0].id)