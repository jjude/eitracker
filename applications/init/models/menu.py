# coding: utf8

#########################################################################
## App Title, description & menus
#########################################################################

response.title = T('Income & Expense Tracker')
response.subtitle = T('do you know how your money comes & goes?')
response.keywords = 'slow wealth, finance, Python, web2py, appengine'
response.description = 'track your expenses and income'

_f = request.function
approot='/init/default/'
if not auth.user:
    response.menu = [(T('login'), True, '%slogin' % approot)]
    response.menu.append((T('register'), False, '%sregister' % approot))
else:
    response.menu = [(T('logout'), False, '%slogout' % approot)]
                          
if auth.is_logged_in():
    response.menu.append((T('profile'), _f=='profile','%sprofile' % approot))
    response.menu.append((T('add income'), _f=='add_income','%sadd_income' % approot))
    response.menu.append((T('add expense'), _f=='add_expense','%sadd_expense' % approot))
    response.menu.append((T('reports'), _f=='reports','%sreports' % approot))
    if auth.has_membership(db((db.auth_group.role=='admin')).select(db.auth_group.ALL)[0].id,auth.user.user_id):
        response.menu.append((T('settings'), _f=='settings','%ssettings' % approot))

response.menu.append((T('help'), False, '/'))