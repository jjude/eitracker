#!/usr/bin/python
# -*- coding: utf-8 -*-
exec("import applications.%s.modules.apihelper as api" % request.application)

record_types = ['Expense','Income']
income_categories=[OPTION(x.name, _value=x.id) for x in db(db.category.is_income=='T').select(db.category.ALL)]
expense_categories=[OPTION(x.name, _value=x.id) for x in db(db.category.is_income=='F').select(db.category.ALL)]
paid_by=[OPTION(x.paid_by, _value=x.id) for x in db().select(db.paid_by.ALL)]

# ###########################################################
# ## controller functions
# ###########################################################
def index():
    return dict()

def login(): 
    return dict(form=auth.login(next='index'))

def logout():
    return dict(form=auth.logout(next='index'))

def register():
    return dict(form=auth.register(next='index'))
    
def profile():
    return dict(form=auth.profile())

@auth.requires_login()
def add_income():
	"""
	Add income details
	"""
	form=FORM(TABLE(
			TR("Category:", SELECT(_name='category', *income_categories)),
			TR("Amount:", INPUT(_type='text', _name='amount')),
			TR("Date: (yyyy-mm-dd)", INPUT(_type='text', _name='recdate')),
            TR("Description:", TEXTAREA(_name='description', rows="2")),
            TR("Created By:", auth.user.email),
            TR("",INPUT(_type="submit", _value=" Add "))
        )
        )
	if form.accepts(request.vars, session):
        #insert into the record via api
		uid = auth.user.id
		api.add_income(db=db,
			record_date=form.vars.recdate, 
            description=form.vars.description, 
            amount=form.vars.amount,
            category=form.vars.category,
            created_on=request.now,
            created_by=uid,
            created_via='web'
			)
		redirect(URL(r=request, f='index'))        
	return dict(addform=form)

@auth.requires_login()
def add_expense():
	"""
	Add expense details
	"""
	form=FORM(TABLE(
			TR("Category:", SELECT(_name='category', *expense_categories)),
			TR("Amount:", INPUT(_type='text', _name='amount')),
			TR("Date: (yyyy-mm-dd)", INPUT(_type='text', _class='date', _name='recdate', _id='recdate')),
			TR("Paid By:", SELECT(_name='paid_by',*paid_by)),
            TR("Description:", TEXTAREA(_name='description', rows="2")),
            TR("Created By:", auth.user.email),
            TR("",INPUT(_type="submit", _value=" Add "))
        )
        )
	if form.accepts(request.vars, session):
        #insert into the record via api
		uid = auth.user.id
		api.add_expense(db=db,
			record_date=form.vars.recdate, 
            description=form.vars.description, 
            amount=form.vars.amount,
            paid_by=form.vars.paid_by,
            category=form.vars.category,
            created_on=request.now,
            created_by=uid,
            created_via='web'
			)
		redirect(URL(r=request, f='index'))        
	return dict(addform=form)
	
@auth.requires_login()
def reports():
	created_by=auth.user.id
	eirecords=db(db.eirecord.created_by==created_by).select(db.eirecord.ALL,orderby=db.eirecord.rec_date)
	rec_bal=0.0
	records=[]
	for record in eirecords:
		rec_bal=rec_bal+record.amount if record.is_income else rec_bal-record.amount
		records.append(dict(
			rec_date=record.rec_date,
			description=record.description,
			income=record.amount if record.is_income else '',
			expense=record.amount if not record.is_income else '',
			balance=rec_bal
			))
	return dict(records=records)
        
#ref: http://groups.google.ca/group/comp.lang.python/msg/a12263a870f5f236
#http://g33k.wordpress.com/2006/07/31/check-gmail-the-python-way/
@auth.requires_login()
def read_and_process_email():
	import poplib
	me='id804097@gmail.com'
	p='Bella1ndia'
	host='pop.gmail.com'
	pop=poplib.POP3_SSL(host)
	try:
		pop.user(me)
		pop.pass_(p)
		message_count,mailbox_size=pop.stat()
	finally:
		pop.quit()
	print message_count,mailbox_size
