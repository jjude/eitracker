#it is not easy to import one controller fns in another controllers; but its easy to import modules
#db instance is not visible to modules; so db should be explicitly passed

def index(): return dict(message="hello from api helper.py")

def add_record(db,
		record_date,
		description,
		amount,
		is_income,
		category,
		paid_by,
		created_on,
		created_by,
		created_via):
	db.eirecord.insert(rec_date=record_date,
		description=description,
		amount=amount,
		is_income=is_income,
		category=category,
		paid_by=paid_by,
		created_on=created_on,
		created_by=created_by,
		created_via=created_via)
		

def add_expense(db,
	record_date,
	description,
	amount,
	category,
	paid_by,
	created_on,
	created_by,
	created_via):
	add_record(db=db,
		record_date=record_date,
		description=description,
		amount=amount,
		is_income=0,
		category=category,
		paid_by=paid_by,
		created_on=created_on,
		created_by=created_by,
		created_via=created_via)
		
def add_income(db,
	record_date,
	description,
	amount,
	category,
	created_on,
	created_by,
	created_via):
	add_record(db=db,
		record_date=record_date,
		description=description,
		amount=amount,
		is_income=1,
		category=category,
		paid_by=3, #as of now paid by for income is not included; it is defaulted to cheque
		created_on=created_on,
		created_by=created_by,
		created_via=created_via)		